#!/bin/bash
# vim: et sts=4 sw=4

set -eu

KALI_PACKAGES_DIR=~/kali/src/packages
KALI_TOOLS_DIR=~/kali/src/tools

USAGE="Usage: $(basename $0) ACTION

Bulk actions on all Kali package repositories.

ACTIONS

  gpgsign  Enable gpgsign for all Git repositories.
  prune    Remove Git repositories that don't exist on GitLab.
  update   Update (ie. pull) all Git repositories.
"

bold() { tput bold; echo "$@"; tput sgr0; }
fail() { echo "$@" >&2; exit 1; }
vrun() { bold "$" "$@"; "$@"; }

confirm() {
    local question=${1:-"Do you want to continue?"}
    local answer=
    local choices=
    local default=yes

    # Discard characters on stdin
    while read -r -t 0; do read -n 256 -r -s; done

    [ $default = yes ] && choices="[Y/n]" || choices="[y/N]"
    read -r -p "$question $choices " answer
    [ "$answer" ] && answer=${answer,,} || answer=$default
    case $answer in
        (y|yes) return 0 ;;
        (*)     return 1 ;;
    esac
}

check_git_tree() {
    local expected_branch=$1
    local branch=
    local diff=
    local status=
    local n_uncommited=
    local n_unstaged=
    local n_untracked=

    # Check that we are on the expected branch
    branch=$(git symbolic-ref --short HEAD)
    if [ "$branch" != "$expected_branch" ]; then
        echo "Wrong Git branch '$branch', expected '$expected_branch'." >&2
        return 1
    fi

    # Check that there's no diff with origin
    diff=$(git diff origin/$branch $branch)
    if [ "$diff" ]; then
        echo "Git diff detected between origin/$branch and $branch." >&2
        return 1
    fi

    # Discard any changes on mrconfig, as we're about to update it anyway
    git checkout --quiet mrconfig

    # Check that the git tree is clean, ref:
    # https://stackoverflow.com/a/2658301/776208
    status=$(git status --porcelain)
    n_uncommited=$(echo "$status" | grep "^M" | wc -l)
    n_unstaged=$(echo "$status" | grep "^ M" | wc -l) 
    n_untracked=$(echo "$status" | grep "^??" | wc -l)
    if [ $n_uncommited != 0 ]; then
        echo "Git tree dirty: $n_uncommited uncommited files." >&2
        return 1
    fi
    if [ $n_unstaged != 0 ]; then
        echo "Git tree dirty: $n_unstaged unstaged files." >&2
        return 1
    fi
    if [ $n_untracked != 0 ]; then
        echo "Git tree dirty: $n_untracked untracked files." >&2
        return 1
    fi
}

update_mr_config() {
    local expected_branch=master
    local mrconfig=$KALI_PACKAGES_DIR/.mrconfig
    local branch=
    local diff=

    # Bail out if .mrconfig was modified less than an hour ago
    if [ -e $mrconfig ]; then
        if [ $(( $(date +%s) - $(stat -c %Y $mrconfig) )) -le 3600 ]; then
            echo "The file .mrconfig is up to date" >&2
            return
        fi
    fi

    # Update .mrconfig
    echo "Updating .mrconfig ..."
    vrun pushd $KALI_TOOLS_DIR/packaging
    if ! check_git_tree master; then
        echo "Cowardly refusing to update the mrconfig!" >&2
        echo "Please cd into $KALI_TOOLS_DIR/packaging, cleanup, push changes, etc..." >&2
        fail "Aborting."
    fi
    vrun git pull --rebase
    vrun ./bin/update-mrconfig
    vrun cp mrconfig $mrconfig
    vrun popd
}

remove_unconfigured() {
    local configured_repos=
    local local_dirs=
    local remove_list=
    local d=

    # List repos that appear in the .mrconfig
    [ -e .mrconfig ] || fail "No .mrconfig file found."
    configured_repos=$(sed -nE 's/^\[(.*)\]/\1/p' .mrconfig \
        | grep -v '^DEFAULT$' | sort -u)

    # List directories in .
    local_dirs=$(find -maxdepth 1 -type d | sed 's;\./;;' \
        | grep -v '^\.$' | sort -u)

    # Keep those directories that don't appear in .mrconfig
    echo "$configured_repos" > .configured.1
    echo "$local_dirs" > .local.1
    remove_list=$(comm -13 .configured.1 .local.1)
    rm -f .configured.1 .local.1

    # Bail out if there's nothing to remove
    if [ -z "$remove_list" ]; then
        echo "No repo to remove, nothing to do."
        return 0
    fi

    # Remove, after confirmation
    echo "Local repositories to remove: " $remove_list
    confirm || return 0
    for d in $remove_list; do
        echo "→ Removing $d ..."
        rm -fr "$d"
    done
}

enable_gpgsign() {
    local d=
    local n=0

    for d in */; do
        [ -d "$d"/.git ] || continue
        cd "$d"
        if [ "$(git config commit.gpgsign)" != true ]; then
            echo "→ Enabling gpgsign for $(basename $(pwd))"
            git config commit.gpgsign true
            (( n+=1 ))
        fi
        cd ..
    done

    if [ $n -eq 0 ]; then
        echo "Nothing done, gpgsign was enabled for all repos already."
    else
        echo "Done, gpgsign was enabled for $n repo(s)."
    fi
}

# main

[ $# -eq 1 ] || fail "$USAGE"
ACTION=$1

cat << EOF
Packages dir: $KALI_PACKAGES_DIR
Tools dir   : $KALI_TOOLS_DIR
EOF

case $ACTION in
    (gpgsign)
        vrun cd $KALI_PACKAGES_DIR
        enable_gpgsign
        ;;
    (prune)
        update_mr_config
        vrun cd $KALI_PACKAGES_DIR
        remove_unconfigured
        ;;
    (update)
        update_mr_config
        vrun cd $KALI_PACKAGES_DIR
        vrun mr --minimal update
        ;;
    (*)
        fail "$USAGE"
        ;;
esac
